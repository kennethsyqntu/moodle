<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Template configuraton file for bitbucket pipelines
 * Duplicate of the GitHub Actions workflow
 *
 * @package    core
 * @copyright  2020 onwards Eloy Lafuente (stronk7) {@link https://stronk7.com}
 * @license    https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = getenv('dbtype');
$CFG->dblibrary = 'native';
$CFG->dbhost    = '127.0.0.1';
$CFG->dbname    = 'test';
$CFG->dbuser    = 'test';
$CFG->dbpass    = 'test';
$CFG->prefix    = 'm_';
$CFG->dboptions = ['dbcollation' => 'utf8mb4_bin'];

$CFG->behat_dbtype    = getenv('dbtype');
$CFG->behat_dblibrary = 'native';
$CFG->behat_dbhost    = '127.0.0.1';                      // Database IP
$CFG->behat_dbname    = 'test';              // Database Schema for PHPUnit (MAKE SURE THIS EXISTS AS AN EMPTY DB)
$CFG->behat_dbuser    = 'test';
$CFG->behat_dbpass    = 'test';
$CFG->behat_dataroot = realpath(dirname(__DIR__)) . '/moodledata/moodle/behat';
$CFG->behat_prefix = 'bht_';
$CFG->behat_wwwroot = 'http://localhost';


$host = '127.0.0.1';
$CFG->wwwroot   = "http://{$host}";
$CFG->dataroot  = realpath(dirname(__DIR__)) . '/moodledata';
$CFG->admin     = 'admin';
$CFG->directorypermissions = 0777;

// Debug options - possible to be controlled by flag in future.
$CFG->debug = (E_ALL | E_STRICT); // DEBUG_DEVELOPER.
$CFG->debugdisplay = 1;
$CFG->debugstringids = 1; // Add strings=1 to url to get string ids.
$CFG->perfdebug = 15;
$CFG->debugpageinfo = 1;
$CFG->allowthemechangeonurl = 1;
$CFG->passwordpolicy = 0;
$CFG->cronclionly = 0;
$CFG->pathtophp = getenv('pathtophp');

$CFG->phpunit_dataroot  = realpath(dirname(__DIR__)) . '/phpunitdata';
$CFG->phpunit_prefix = 't_';

define('TEST_EXTERNAL_FILES_HTTP_URL', 'http://127.0.0.1');
define('TEST_EXTERNAL_FILES_HTTPS_URL', 'http://127.0.0.1');

define('TEST_SESSION_REDIS_HOST', '127.0.0.1');
define('TEST_CACHESTORE_REDIS_TESTSERVERS', '127.0.0.1');

// TODO: add others (solr, mongodb, memcached, ldap...).

// Too much for now: define('PHPUNIT_LONGTEST', true); // Only leaves a few tests out and they are run later by CI.

$CFG->behat_config = array(
  'Mac-Firefox' => array(
	  'suites' => array (
		  'default' => array(
			  'filters' => array(
				 'tags' => '~@_file_upload'
			  ),
		  ),
	  ),
	  'extensions' => array(
		  'Behat\MinkExtension' => array(
			  'selenium2' => array(
				  'browser' => 'firefox',
				  'capabilities' => array(
					  'platform' => 'OS X 10.6',
					  'version' => 20
				  )
			  )
		  )
	  )
  ),
  'Mac-Safari' => array(
	  'extensions' => array(
		  'Behat\MinkExtension' => array(
			  'selenium2' => array(
				  'browser' => 'safari',
				  'capabilities' => array(
					  'platform' => 'OS X 10.8',
					  'version' => 6
				  )
			  )
		  )
	  )
  )
);
$CFG->behat_profiles = [
        'default' => [
            'browser' => 'chrome',
            'wd_host' => 'http://selenium:4444/wd/hub',
            'extensions' => [
                'Behat\MinkExtension' => [
                    'selenium2' => [
                        'browser' => 'chrome',
                    ]
                ]
            ]
        ]
    ];


require_once(__DIR__ . '/lib/setup.php');
